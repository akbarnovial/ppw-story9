function bookSearch() {
  var search = document.getElementById('searchbutton').value
  document.getElementById('results').innerHTML = ""
  console.log(search)

  $.ajax({
    url: "https://www.googleapis.com/books/v1/volumes?q=" + search,
    dataType: "json",

    success: function(data){
      console.log(data)
    },

    type: 'GET'
  });
}

document.getElementById('searchbutton').addEventListener('click', bookSearch, false)
