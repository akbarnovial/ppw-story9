function bookSearch() {
  var search = document.getElementById('search').value
  document.getElementById('results').innerHTML = ""
  document.getElementById('top5').innerHTML = ""


  $.ajax({
    url: "https://www.googleapis.com/books/v1/volumes?q=" + search,
    dataType: "json",

    success: function(data){
      var titleCount = 0;
      results.innerHTML += "<tbody>"
      results.innerHTML += "<tr>" + "<th>" + "Title" + "</th>" + "<th>" + "Author" + "</th>" + "<th>" + "Likes" + "</th>" + "</tr>"
      for (i=0; i < data.items.length; i++){
        results.innerHTML += "<tr>" + "<td id ='title" + i + "'>" + data.items[i].volumeInfo.title + "</td>" + "<td>" + data.items[i].volumeInfo.authors[0] + "</td>" + "<td><p id='likes" + i + "'>0</p><button id='button" + i + "' onclick=addLikes('likes" + i + "');sortBooks('title" + i + "','likes" + i + "')>Like</button></td>" + "</tr>"
        if (titleCount<5){
          top5.innerHTML += "<p id='top5Title" + i + "'>" + data.items[i].volumeInfo.title + "</p>" + "<p id='top5Likes" + i + "'>0</p>"
        }titleCount++
      }
      results.innerHTML += "</tbody>"
    },

    type: 'GET'
  });
}

function addLikes(e){
  var currLike = parseInt(document.getElementById(e).textContent)
  document.getElementById(e).innerHTML = currLike + 1
  console.log(currLike)
}

document.getElementById('search').addEventListener('keypress', function(e){
  if (e.keyCode == 13) {
    bookSearch();
  }
}, false)

function sortBooks(id, likes){
  var title1, title2, tempPosition
  var currLikes= parseInt(document.getElementById(likes).textContent)
  var currTitle= document.getElementById(id).textContent
  var lowestLikes = parseInt(document.getElementById('top5Likes4').textContent)

  if(document.getElementById('top5').innerHTML.indexOf(currTitle) !== -1){
    for (i = 4; i >= 0; i--){
      if(document.getElementById('top5Title'+i).textContent == currTitle){
        document.getElementById('top5Likes'+i).textContent++
      }
    }
  }else{
    if (currLikes>lowestLikes){
      document.getElementById('top5Title4').textContent = currTitle
      document.getElementById('top5Likes4').textContent = currLikes

    }
  }

  for (i = 4; i > 0; i--) {

    title1 = parseInt(document.getElementById('top5Likes'+i).textContent)
    title2 = parseInt(document.getElementById('top5Likes'+(i-1)).textContent)

    if (title1 > title2) {
      tempPosition = document.getElementById('top5Title'+i).textContent
      document.getElementById('top5Title'+i).textContent = document.getElementById('top5Title'+(i-1)).textContent
      document.getElementById('top5Title'+(i-1)).textContent = tempPosition
      document.getElementById('top5Likes'+(i-1)).textContent = title1
      document.getElementById('top5Likes'+i).textContent = title2
    }
  }
}

function updateModal(){
  var modal = document.getElementById('modal-body')
  if (document.getElementById('top5').innerHTML.indexOf("yeetyeetyeetyeetyeet123456789") == -1) {
    modal.innerHTML = ""
    modal.innerHTML += "<ol>"
    for (i=0; i<5; i++) {
      modal.innerHTML += "<li id='topBook"+ i +"'>" + document.getElementById('top5Title'+i).textContent + "</li>"
  }
  modal.innerHTML += "</ol>"
  }
}
