from django.test import TestCase, LiveServerTestCase
from django.test.client import Client
from django.apps import apps
from tugas.apps import TugasConfig
from django.http import HttpRequest
from django.urls import resolve
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from .views import index
import time


# Create your tests here.
class TestUnit(TestCase):

    def test_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)

    def test_template_used(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'pages/index.html')

    def test_app(self):
        self.assertEqual(TugasConfig.name, 'tugas')
        self.assertEqual(apps.get_app_config('tugas').name, 'tugas')

    def test_title(self):
        response = Client().get('/')
        response_content = response.content.decode('utf-8')
        self.assertIn("Library", response_content)

    def test_resolve_function(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

class FunctionalTestLMAO(LiveServerTestCase):
    def setUp(self):
        super().setUp()
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        chrome_options.add_argument('--disable-dev-shm-usage')

        self.browser= webdriver.Chrome(chrome_options=chrome_options, executable_path = 'chromedriver')
        # self.browser= webdriver.Firefox()
    def tearDown(self):
        self.browser.quit()

    def test_search(self):
        self.browser.get(self.live_server_url)
        time.sleep(2)
        search = self.browser.find_element_by_id('search')
        button = self.browser.find_element_by_id('searchbutton')
        search.send_keys('percy')
        time.sleep(2)

        button.click()
        time.sleep(5)

        self.assertIn('Title', self.browser.page_source)
        time.sleep(2)

    def test_like(self):
        self.browser.get(self.live_server_url)
        time.sleep(2)
        search = self.browser.find_element_by_id('search')
        button = self.browser.find_element_by_id('searchbutton')
        search.send_keys('percy')
        time.sleep(3)

        button.click()
        time.sleep(3)
        likeButton = self.browser.find_element_by_id('button0')
        likeButton.click()
        time.sleep(3)

        likes = self.browser.find_element_by_id('likes0').text
        self.assertEqual(likes, '1')

    def test_top_5_books(self):
        self.browser.get(self.live_server_url)
        time.sleep(2)
      
        search = self.browser.find_element_by_id('search')
        button = self.browser.find_element_by_id('searchbutton')
        search.send_keys('percy')
        time.sleep(3)

        # button.send_keys(Keys.RETURN)
        button.click()
        time.sleep(5)

        title = self.browser.find_element_by_id('title1').text
        likeButton1 = self.browser.find_element_by_id('button1')
        likeButton1.click()
        likeButton1.click()
        time.sleep(3)

        modalButton = self.browser.find_element_by_id('modalButton')
        modalButton.click()
        time.sleep(3)

        topBook = self.browser.find_element_by_id('topBook0').text
        self.assertEqual(title, topBook)
